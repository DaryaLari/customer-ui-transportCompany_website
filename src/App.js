import React, { Component } from 'react';
import Header from 'js/Elements/Common/Header'

 class App extends Component {
  render() {
    return (
     <div id="app">
       <Header />
     </div>
    );
  }
}

export default App;