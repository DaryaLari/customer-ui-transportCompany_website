import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from 'registerServiceWorker'
import Routing from 'settings/Routing'

import configureStore, { history } from 'settings/configureStore'
import 'index.css'

export const store = configureStore();

ReactDOM.render(
  <Routing store={store} history={history} />,
  document.getElementById('root')
);

registerServiceWorker();
