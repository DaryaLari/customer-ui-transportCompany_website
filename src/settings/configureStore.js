/*
* Sets up the app's storage and reducers (events' handlers)
*/

import { createStore, applyMiddleware } from 'redux'
import rootReducer from 'reducers'
import thunk from 'redux-thunk';/* for asynchronous ations */
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux'
import createBrowserHistory from 'history/createBrowserHistory';

import logger from 'redux-logger'
import INIT_STATE from 'settings/storeInitState';

export const history = createBrowserHistory();

export default function configureStore() {

  const store = createStore(
    rootReducer,
    INIT_STATE,
    composeWithDevTools(applyMiddleware(thunk, logger, routerMiddleware(history))));

syncHistoryWithStore(history, store);
  
  if (module.hot) {
      module.hot.accept('../reducers', () => {
        const nextRootReducer = require('../reducers')
        store.replaceReducer(nextRootReducer)
      })
  }

  return store;
}
