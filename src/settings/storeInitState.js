/*
* Initial states of the app's storage entities
*/

export const authInitState = {
  user: JSON.parse(localStorage.getItem("signup.user")),
  fetching: false,
  message: null,
  nonce: 3,
}


export const parcelsListInitState = {
	parcels: [],
	fetching: false,
	fetched: false,
	message: null
}

export const routingInitState = {
  locationBeforeTransitions : null
}

export const requestDeliveryInitState = {
  success: null,
  fetching: false,
  message: null
}

export const submitFeedbackInitState = {
  success: null,
  fetching: false,
  message: null
}

export const archivedParcelsListInitState = {
	parcels: [],
	fetching: false,
	fetched: false,
	message: null
}

const INIT_STATE = {
  routing: routingInitState,
  authorization: authInitState,
  parcelsList: parcelsListInitState,
  requestDelivery: requestDeliveryInitState,
  submitFeedback: submitFeedbackInitState,
  archivedParcelsList: archivedParcelsListInitState
}

export default INIT_STATE;
