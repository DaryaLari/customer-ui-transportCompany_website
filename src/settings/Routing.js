/*
* Manages navigation of the app
*/

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Provider } from 'react-redux'
import { Redirect, Router, Route } from 'react-router-dom'
import Switch from 'react-router/Switch'
import { store } from 'index'

import App from 'App'
import HomePage from 'js/Pages/HomePage'
import SignUpPage from 'js/Pages/SignUpPage'
import SignInPage from 'js/Pages/SignInPage'
import ParcelsPage from 'js/Pages/ParcelsPage'
import ArchivedParcelsPage from 'js/Pages/ArchivedParcelsPage'
import DeliverRequestPage from 'js/Pages/DeliverRequestPage'

var Paths = require('constants/AppPaths').PagesPaths;

const requireSignin = (x) => () => store.getState().authorization.user ? x : <Redirect to={Paths.SignInPage}/>;
 
const requireSignout = (x) => () => !store.getState().authorization.user ? x : <Redirect to={Paths.ParcelsPage}/>;

class Routing extends Component {
   render() {
     return (
        <Provider store={this.props.store}>
            <Router history={this.props.history}>
            <div>
                <Route path={Paths.App} component={App} />
                <Switch>
                    <Route exact path={Paths.HomePage} component={HomePage} />
                    <Route exact path={Paths.SignInPage} render={requireSignout(<SignInPage/>)} />}
                    <Route exact path={Paths.SignUpPage} render={requireSignout(<SignUpPage/>)} />
                    <Route exact path={Paths.ParcelsPage} render={requireSignin(<ParcelsPage/>)} />
                    <Route exact path={Paths.ArchivedParcelsPage} render={requireSignin(<ArchivedParcelsPage/>)} />
                    <Route exact path={Paths.DeliveryRequestPage} render={requireSignin(<DeliverRequestPage/>)} />
                    <Route exact path="*" render={() => <Redirect to={Paths.HomePage} />} />
                </Switch>
                </div>
            </Router>
        </Provider>
     );
   }
}

export default connect(
state =>({}),
dispatch =>({})
)(Routing);
