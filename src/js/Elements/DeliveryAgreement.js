import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Input, Button } from 'react-materialize'

import { ButtonProps, InputProps } from 'constants/StylesCSS'

 class DeliveryAgreement extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            agreementAccepted: false,
            requestConfirmed: false
        };
        this.submitRequest = this.submitRequest.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount(){
        window.$('#' + this.props.modalId).modal('open');
    }
    handleChange(event){
        this.setState({agreementAccepted: event.target.checked});
    }
    submitRequest(){
        this.props.onAcceptAgreement();
        window.$('#delivery-agreement').modal('close');
    }
    render() {
        return (
        <Modal id={this.props.modalId}
            header="Delivery Agreement"
        >
            <div className="container center-align">
                <p>By confirming request you agree with <a href="https://goo.gl/3G6DvC">Data Procesing Policy</a> of company <b>"DeliverMe"</b></p>
            </div>
            <div className="container center-align">
                <Input {...InputProps}
                    key="delivery-agreement-confirmation"
                    type="checkbox"
                    label="I agree with delivery agreement" 
                    onChange={this.handleChange} 
                />
                <br/>
            </div>
            <div className="container center-align">
                <Button 
                    {...ButtonProps}
                    disabled={!this.state.agreementAccepted}
                    onClick={this.submitRequest} 
                >
                    Confirm Delivery Request
                </Button>
            </div>
        </Modal>
        );
    }
}

export default connect(
  state => ({
  }),
  dispatch => ({
  })
)(DeliveryAgreement);