import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Preloader } from 'react-materialize'

class LoadingItem extends Component {
    render() {
        return (
            <div id="loader" >
                { // exists by default, hidden only when 'fetching==false'
                    (this.props.fetching || this.props.fetching == null) &&
                    <Preloader flashing/>
                }
                </div>
        );
    }
}
 
export default connect(
    state =>({}),
    dispatch =>({})
)(LoadingItem);