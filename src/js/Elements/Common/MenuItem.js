import React, { Component } from 'react';


var NavLink = require('react-router-dom').NavLink;


class MenuItem extends Component {
   render() {
     return (
      <div className={"container "+this.props.className}>
              <NavLink exact to={this.props.path}>{this.props.name}</NavLink>
      </div>
     );
   }
}

export default MenuItem;
