import React, { Component } from 'react'
import {Navbar, NavItem} from 'react-materialize'
import { connect } from 'react-redux'
import { signOut } from 'actions/dispatchedActions/index'
import { exclusive } from 'actions/utils'

const appPaths = require('constants/AppPaths').PagesPaths;

 class Header extends Component {
  render() {
    return (
     <div className="header">
       <Navbar className="left-align" brand={<div className="container valign-wrapper"><img src="/logo.png" alt="" /></div>} right>
         <NavItem href={appPaths.DeliveryRequestPage}>Request Delivery</NavItem>
         <NavItem href={appPaths.ParcelsPage}>Active Parcels</NavItem>
         <NavItem href={appPaths.ArchivedParcelsPage}>Archived Parcels</NavItem>
         {this.props.user ? 
           <NavItem onClick={exclusive()}> {this.props.user.email}</NavItem>:null}
         {this.props.user ? 
           <NavItem onClick={exclusive(this.props.onSignOutButton)}>Sign Out</NavItem>:
           <NavItem href={appPaths.SignInPage}>Sign In / Sign Up</NavItem>
         }
      </Navbar>
     </div>
    );
  }
}

export default connect(
  state => ({
    user: state.authorization.user,
    errorMessage: state.authorization.message
  }),
  dispatch => ({
    onSignOutButton: () => dispatch(signOut()),
  })
)(Header);