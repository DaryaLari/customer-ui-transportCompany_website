import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {Button, Input} from 'react-materialize'
import { ButtonProps, InputProps } from 'constants/StylesCSS'

import LoadingItem from 'js/Elements/Common/LoadingItem'

var approve = require('approvejs');

class InputForm extends Component {
    autocomplete = {};
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.clickSubmit = this.clickSubmit.bind(this);
        this.getAutocompletedData = this.getAutocompletedData.bind(this);
        this.onPlaceSelected = this.onPlaceSelected.bind(this);
        this.state = {
            values: {},
            errMsg: {},
            error: ""
        };
        let initState = this.state;
        Object.keys(props.inputs).map((key) => {
            let input = props.inputs[key];
            initState.values[key] = input.initValue ? input.initValue : (input.options ? input.options[0] : null);            
            initState.errMsg[key] = null;
            return key;
        })

        this.state = initState;
    }

    componentDidMount(){
        if (!window.google) {
            throw new Error('Cannot load Google Maps JavaScript API library')
        }
        Object.keys(this.props.inputs).map((key) => {
            if(this.props.inputs[key].autocomplete === 'places')
            {
                let input = ReactDOM.findDOMNode(this.refs[key]).children[key];
                this.autocomplete[key] = new window.google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(input),
                {types: ['geocode']});

                this.autocomplete[key].addListener('place_changed', () => this.onPlaceSelected(key));
            }
            return key;
        })
    }

    // on change of input value in any form
    handleChange(event){
        const newState = this.state;
        const target = event.target;
        const key = target.name;
        const inputProps = this.props.inputs[key];
        if (this.autocomplete[key]) 
        {
            switch(this.props.inputs[key].autocomplete){
                case 'places': {
                    this.autocomplete[key].set('place', null);
                    break;
                }
                default: {
                    break;
                }
            }
        }
        newState.values[key] = target.type === "checkbox" ? target.checked : target.value.trim();
        this.setState(newState);
    }

    // on manually choosing the place from suggestions (by mouse click or pressing 'Enter')
    onPlaceSelected(key){
        // console.log(this.autocomplete[key].getPlace())
        const newState = this.state;
        newState.values[key] = this.autocomplete[key].getPlace() ? 
            this.autocomplete[key].getPlace()
            : null;
        this.setState(newState);
        // console.log(this.state.values)
    }

    clickSubmit(){    
        const validationResult = {};
        const values = this.state.values;
        const errors = {};
        Object.keys(this.refs).map((key) => {
            const inputProps = this.props.inputs[key];
            validationResult[key] = this.validate(values[key], inputProps.validationRules, key)
            if(!validationResult[key].approved){
                errors[key] = this.specifyError(inputProps.validationRules, validationResult[key]);
            }
            return key;
        }); 
        Object.keys(validationResult).every((key) => {return validationResult[key].approved}) ?
            this.props.onSubmit(values)
        :   this.setState({errMsg: errors, error: "Fill fields with valid data"})
    }

    render() {
        return (
            <div className="container">
        
                <h4>{this.props.title}</h4>

                <div>
                    {Object.keys(this.props.inputs).map((key) => {
                        let input = this.props.inputs[key];
                        return (<Input {...InputProps}
                            children={this.expandOptions(input.options)}
                            key={key}
                            type={input.type} 
                            defaultValue={input.initValue ? input.initValue : (input.options ? input.options[0] : null)}
                            name={key} 
                            label={input.label} 
                            onChange={this.handleChange} 
                            placeholder=""
                            disabled={this.props.disabled} 
                
                            ref={key}
                
                            error={this.state.errMsg[key] ? this.state.errMsg[key] : ""}
                        />)
                    })}
                    <p className="errorMessage">{this.state.error}</p>
                    {
                        this.props.fetching &&
                        <LoadingItem />
                    }
                    {
                        !this.props.hiddenBtn && !this.props.fetching &&
                        <Button 
                            {...ButtonProps} 
                            className={this.props.button.className}
                            onClick={this.clickSubmit} 
                            disabled={this.props.disabled ? this.props.disabled : false}
                        >
                            {this.props.button.name ? this.props.button.name : "Submit"}
                        </Button> 
                    }
                </div>        
            </div>
        );
    }

    // returns data stored as selected in autocomplete entity
    getAutocompletedData(key){
        switch(this.props.inputs[key].autocomplete){
            case 'places':
                {return this.autocomplete[key].getPlace();}
            default: {}
        }
        return;
    }
    
    // convert array of string names, 
    // used for discribing possible input options,
    // to a form, understandable by 'Input' component
    expandOptions(options){
        if(!options) return null;
        var result = options.map(
            (option, index) => {
                return (
                    <option key={index} value={option}>
                        {option}
                    </option>
                )
            }
        );
        return result;
    }

    // check validity of data in input field
    validate(value, rule, key){
        const val = this.autocomplete[key] ?
            this.getAutocompletedData(key)
            : value;
        return rule ? 
            approve.value(val, rule)
        : {
            approved: true,
            errors: [],
            failed: []
        }
    }

    // returns a string error text for invalid data in input field
    specifyError(rule, error){
        return  rule ?
        Object.keys(rule).map((key) => {
            if (!error[key].approved){
                return error[key].errors.filter(x => x).join(". ");
            }
            return null;
        }).filter(x => x).join(". ")
        : null
    }
}

export default connect(
  state => ({}),
  dispatch => ({}),
)(InputForm);
