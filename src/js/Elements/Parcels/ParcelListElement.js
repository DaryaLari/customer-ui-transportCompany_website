import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, CardTitle, Col, Row } from 'react-materialize'
import ParcelFeedbackForm from 'js/Elements/Parcels/ParcelFeedbackForm'

const deliveryStatus = require('constants/Statuses').deliveryStatus;
const boldHeaderColProps = {
  s: 2,
  style: {fontWeight: 'bold'},
  className: "right-align"
}
const normalColProps = {
  s: 10,
  className: "left-align"
}
class ParcelListElement extends Component {
  constructor(props) {
    super(props);
    this.handleExpansion = this.handleExpansion.bind(this);
    this.handleGetCode = this.handleGetCode.bind(this);
  
    this.state = {
        expanded: false,
        confirmationCode: null
    };
  }

  handleExpansion(){
    this.setState({expanded: !this.state.expanded});
  }

  handleGetCode(event){
    this.setState({confirmationCode: this.props.parcelInfo.conf_num})
  }


  render() {
    return (
    <Card 
      className='hoverable'
      header={
        <CardTitle
          className="confirm orange lighten-2 left-align" image=""
        >
          <b>
            Order #{this.props.parcelInfo.id}
          </b>
          <i>
            [{deliveryStatus[this.props.parcelInfo.status].description}]
          </i>
        </CardTitle>
      }
      actions={[<a onClick={this.handleExpansion} key="info" style={{cursor: 'pointer'}}>{this.state.expanded ? 'Hide' : 'Expand'}</a>]}
    >
      <Row>
        <Col s={8}>
          <div>
            <Row>
              <Col {...boldHeaderColProps}>For</Col>
              <Col {...normalColProps}>{this.props.parcelInfo.name}</Col>
            </Row>
            <Row>
              <Col {...boldHeaderColProps}>From</Col>
              <Col {...normalColProps}>{this.props.parcelInfo.source}</Col>
            </Row>
            <Row>
              <Col {...boldHeaderColProps}>To</Col>
              <Col {...normalColProps}>{this.props.parcelInfo.destination}</Col>
            </Row>
          </div>
        </Col>
        <Col s={3}>
          {this.props.parcelInfo.status === deliveryStatus.PickedUp.statusText &&
            (this.state.confirmationCode ?
              (<Card className="orange">
                <b>Confirmation Code:</b><br/>
                {this.state.confirmationCode}
              </Card>)
              : (<Card className="orange hoverable" style={{cursor: 'pointer'}} onClick={this.handleGetCode}>
                  Get confirmation code
                </Card>
                ) 
            )
          }
        </Col>
      </Row>
      <Row>
        <Col s={8}>
          {this.state.expanded &&
            <div>
              <Row>
                <Col {...boldHeaderColProps}>Urgency</Col>
                <Col {...normalColProps}>{this.props.parcelInfo.urgency}</Col>
              </Row>
              <Row>
                <Col {...boldHeaderColProps}>Type</Col>
                <Col {...normalColProps}>{this.props.parcelInfo.type}</Col>
              </Row>
              <Row>
                <Col {...boldHeaderColProps}>Status</Col>
                <Col {...normalColProps}>{deliveryStatus[this.props.parcelInfo.status].description}</Col>
              </Row>
              {this.props.parcelInfo.reject_msg &&
                <Row>
                  <Col {...boldHeaderColProps}>Reject message</Col>
                  <Col {...normalColProps}>{this.props.parcelInfo.reject_msg}</Col>
                </Row>
              }
            </div>
          }
        </Col>
        
      </Row>
      {
        this.state.expanded && this.props.parcelInfo.status === deliveryStatus.Delivered.statusText &&
        <ParcelFeedbackForm parcelId={this.props.parcelInfo.id} />
      }
    </Card>
    );
  }
}

export default connect(
  state =>({
   submitFeedbackState: state.submitFeedback
  }),
  dispatch =>({
    // onSibmitFeedback: (id, mark, msg) => {
    //   dispatch(submitFeedback({
    //     parcel_id: id,
    //     mark: mark,
    //     comment: msg
    //   }))
    // }
  })
)(ParcelListElement);
