import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Collection, CollectionItem } from 'react-materialize'
import ParcelListElement from 'js/Elements/Parcels/ParcelListElement'

class ParcelsList extends Component {
    render() {
        var parcels = this.props.parcels ? this.props.parcels : [];
        parcels = this.props.filterStatus ? parcels.filter(p => this.props.filterStatus.indexOf(p.status) > -1) : parcels;
        parcels = parcels.sort((a, b) => {
            return a.id < b.id ? 1 : -1;
        });
        return ( 
        <div id="parcels-list" className="col s1"> 
            {
                !this.props.fetching && 
                this.props.error ?
                (<p className="errorMessage">{this.props.error}</p>) :
                (parcels && parcels.length ?
                    <Collection>
                        {
                            parcels.map((parcel) => {
                                return(
                                    <CollectionItem key={parcel.id}>
                                        <ParcelListElement key={parcel.id}
                                            parcelInfo={parcel}
                                        />
                                    </CollectionItem>
                                )
                            })
                        }
                    </Collection>
                :
                <h5> There are no parcels yet </h5>)
            } 
         </div>
            );
        }
    }

export default connect(
    state => ({
    }),
    dispatch => ({
    })
)(ParcelsList);