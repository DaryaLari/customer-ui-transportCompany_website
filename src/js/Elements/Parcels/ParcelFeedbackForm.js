import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, Button } from 'react-materialize'
import ReactStars from 'react-stars'
import LoadingItem from 'js/Elements/Common/LoadingItem'
import { submitFeedback } from 'actions/dispatchedActions/index'

class ParcelFeedbackForm extends Component {
    constructor(props) {
      super(props);
      this.handleChangeFeedback = this.handleChangeFeedback.bind(this);
      this.clickSubmitFeedback = this.clickSubmitFeedback.bind(this);
      this.ratingChanged = this.ratingChanged.bind(this);
  
      this.state = {
          feedbackComment: "",
          feedbackMark: 0
      };
    }

    handleChangeFeedback(event){
      this.setState({feedbackComment: event.target.value});
    }
  
    ratingChanged(newRating){
      this.setState({feedbackMark: newRating});
    }
  
    clickSubmitFeedback(){
      this.props.onSibmitFeedback(
        this.props.parcelId,
        this.state.feedbackMark,
        this.state.feedbackComment.trim()
      );
    }
  
    render() {
      let feedbackDisabled = this.props.submitFeedbackState.fetching || this.props.submitFeedbackState.success;
      return (
          <Card
            header={<h5>Your feedback for this delivery</h5>}
          >
            {!feedbackDisabled &&
                <ReactStars
                    count={5}
                    onChange={this.ratingChanged}
                    size={24}
                    color2={'#ffd700'} 
                    value={this.state.feedbackMark}
                    edit={!feedbackDisabled}
                />
            }{feedbackDisabled &&
                <ReactStars
                    count={5}
                    size={24}
                    color2={'#ffd700'} 
                    value={this.state.feedbackMark}
                    edit={false}
                />
            }
            <div className="input-field">
              <textarea 
                className="materialize-textarea"
                id="feedbackField" 
                onChange={this.handleChangeFeedback} 
                disabled={feedbackDisabled}
              />
              <label htmlFor="feedbackField">Feedback</label>
            </div>
            {this.props.submitFeedbackState.success === false &&
              <p className="errorMessage">{this.props.submitFeedbackState.message}</p>
            }
            {this.props.submitFeedbackState.fetching ?
              <LoadingItem fetching={this.props.submitFeedbackState.fetching}/>
            :
              (this.props.submitFeedbackState.success ?
                <h5>Your feedback has been successfully submited!</h5>
                :
                <Button
                  className="orange"
                  onClick={this.clickSubmitFeedback} 
                  disabled={feedbackDisabled || this.state.feedbackComment === "" || this.state.feedbackMark === 0}
                >
                  Submit Feedback
                </Button>
              )
            }
          </Card>
        )
    }
   }
  
  export default connect(
    state =>({
      submitFeedbackState: state.submitFeedback
    }),
    dispatch =>({
      onSibmitFeedback: (id, mark, msg) => {
        dispatch(submitFeedback({
          parcel_id: id,
          mark: mark,
          comment: msg
        }))
      }
    })
  )(ParcelFeedbackForm);
  