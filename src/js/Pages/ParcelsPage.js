import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Collapsible, CollapsibleItem, Input } from 'react-materialize'
import ParcelsList from 'js/Elements/Parcels/ParcelsList'
import LoadingItem from 'js/Elements/Common/LoadingItem'
import { getParcelsList } from 'actions/dispatchedActions/index'
import { extendProps, PageProps } from 'constants/StylesCSS'

const deliveryStatus = require('constants/Statuses').deliveryStatus;

 class ParcelsPage extends Component {
   constructor(props) {
    super(props);
    this.props.onGetParcels();
    this.state = {
      filterStatus: Object.keys(deliveryStatus)
    }
    this.filterChanged = this.filterChanged.bind(this);
  }

  filterChanged(event){
    event.target.checked ? 
      this.setState({filterStatus: this.state.filterStatus.concat(event.target.value)})
    : 
    this.setState({filterStatus: this.state.filterStatus.filter((item) => { 
          return item !== event.target.value;
      })});
  }

  render() {
    return (
    <div id="parcels-page"  {...extendProps(PageProps, "className", " center-align ")}>
      <div id="parcels-menu">
      <h4>Active Parcels</h4>
        <Collapsible>
          <CollapsibleItem header={<h5><i>Filter</i></h5>} className="left-align">
            <b>Status:</b>
            {
              Object.keys(deliveryStatus).map((key) => {
                return (<Input 
                  name="status" 
                  key={key} 
                  type="checkbox" 
                  value={key} 
                  label={deliveryStatus[key].description} 
                  checked={this.state.filterStatus && this.state.filterStatus.includes(key) && "checked"}
                  onChange={this.filterChanged}
                />);
              })
            }
          </CollapsibleItem>
        </Collapsible>
        {
          this.props.fetching ?
          <LoadingItem />
          :
          <ParcelsList 
            error={this.props.error} 
            fetching={this.props.fetching} 
            parcels={this.props.parcels} 
            filterStatus={this.state.filterStatus} 
          />
        }
      </div>
    </div>
    );
  }
 }

 export default connect(
  state =>({
    error: state.parcelsList.message,
    fetching: state.parcelsList.fetching,
    parcels: state.parcelsList.parcels
  }),
  dispatch =>({
    onGetParcels: () => {
      dispatch(getParcelsList());
    }
  })
)(ParcelsPage);
