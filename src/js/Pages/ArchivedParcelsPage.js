import React, { Component } from 'react'
import { connect } from 'react-redux'
import ParcelsList from 'js/Elements/Parcels/ParcelsList'
import LoadingItem from 'js/Elements/Common/LoadingItem'
import { getArchivedParcelsList } from 'actions/dispatchedActions/index'
import { extendProps, PageProps } from 'constants/StylesCSS'

 class ParcelsPage extends Component {
   constructor(props) {
    super(props);
    this.props.onGetParcels();
  }
  render() {
    return (
    <div id="parcels-page"  {...extendProps(PageProps, "className", " center-align ")}>
      <div id="parcels-menu">
      <h4>Archived Parcels</h4>
        {
          this.props.fetching ?
          <LoadingItem />
          :
          <ParcelsList 
          error={this.props.error} 
          fetching={this.props.fetching} 
          parcels={this.props.parcels} 
        />
      }
    </div>
  </div>
  );
}
}

export default connect(
  state =>({
    error: state.archivedParcelsList.message,
    fetching: state.archivedParcelsList.fetching,
    parcels: state.archivedParcelsList.parcels
  }),
  dispatch =>({
    onGetParcels: () => {
      dispatch(getArchivedParcelsList());
    }
  })
)(ParcelsPage);
