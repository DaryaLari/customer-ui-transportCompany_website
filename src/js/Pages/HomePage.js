import React, { Component } from 'react'
import { CardPanel, Row, Col } from 'react-materialize'
import Slider from 'react-slick'
import { extendProps, PageProps } from 'constants/StylesCSS'
 class HomePage extends Component {
  render() {
    const contactsHeadersColStyle = {
      s: 2,
      className: "left-align"
    }
    const contactsDataColStyle = {
      s: 8,
      className: "left-align"
    }
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      draggable: true,
      pauseOnHover: true
    };
    const styles = {
      style : {
        margin: "0px auto",
        padding: "10em 0em",
        backgroundPosition: "center",
        backgroundSize: "contain",
        backgroundRepeat: "no-repeat"
      }
    }
    var style1 = JSON.parse(JSON.stringify(styles));
    style1.style.backgroundImage = "url(/Media/Pictures/slide11.png)";
    var style2 = JSON.parse(JSON.stringify(styles));
    style2.style.backgroundImage = "url(/Media/Pictures/slide10.png)";
    var style3 = JSON.parse(JSON.stringify(styles));
    style3.style.backgroundImage = "url(/Media/Pictures/slide16.jpg)";
    var style4 = JSON.parse(JSON.stringify(styles));
    style4.style.backgroundImage = "url(/Media/Pictures/slide7.png)";
    var style5 = JSON.parse(JSON.stringify(styles));
    style5.style.backgroundImage = "url(/Media/Pictures/slide4.png)";
    return (
      <div id="home-page"  {...extendProps(PageProps, "className", " center-align ")}>
        <h4>Welcome to website of transport company "DeliverMe"</h4>
        <CardPanel>
        <Slider {...settings}>
          <div>
            <h5>Delivery in any directions</h5>
            <div {...style1}></div>
          </div>
          <div>
            <h5>All over the world</h5>
            <div {...style2}></div>
          </div>
          <div>
            <h5>By all kinds of transport</h5>
            <div {...style3}></div>
          </div>
          <div>
            <h5>Any goods</h5>
            <div {...style4}></div>
          </div>
          <div>
            <h5>Directly to the reciever</h5>
            <div {...style5}></div>
          </div>
        </Slider>
        <br/>
        </CardPanel>

        <CardPanel>
          <h4>Our Contacts</h4>
          <div>
            <Row>
              <Col {...contactsHeadersColStyle}>
                <b>E-mail</b>
              </Col>
              <Col {...contactsDataColStyle}>
                <a>support-team@deliverme.com</a>
              </Col>
            </Row>
            <Row>
              <Col {...contactsHeadersColStyle}>
                <b>Phone</b>
              </Col>
              <Col {...contactsDataColStyle}>
                <a>+7 (843) 123-45-67</a>
              </Col>
            </Row>
            <Row>
              <Col {...contactsHeadersColStyle}>
                <b>Registered Address</b>
              </Col>
              <Col {...contactsDataColStyle}>
                420500, Russian Federation, Tatarstan Republic, Innopolis Town, Universiteskaya 1, office 420
              </Col>
            </Row>
          </div>
        </CardPanel>
      </div>
    );
  }
}

export default HomePage;
