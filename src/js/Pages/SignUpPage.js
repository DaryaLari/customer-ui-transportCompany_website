import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Col } from 'react-materialize'
import InputForm from 'js/Elements/Common/InputForm'

import { signUp, initAuthorization } from 'actions/dispatchedActions/index'
import { PageProps } from 'constants/StylesCSS'
import { SignUpFormParams } from 'constants/LogicalConstants'

const SignInPath = require('constants/AppPaths').PagesPaths.SignInPage;

class SignUpPage extends Component {
  
componentWillMount(){
  this.props.onInitAuthState();
}

  render() {
    return (
      <div id="signup-page" {...PageProps}>
          <InputForm 
            {...SignUpFormParams} 
            onSubmit={(params) => {this.props.onSignUpButton(params)}}
            fetching={this.props.fetching}
            disabled={this.props.fetching} 
            hiddenBtn={this.props.fetching} 
          />
          { 
            this.props.errorMessage && 
            <p className="errorMessage">
              {this.props.errorMessage}
            </p>
          }
          <Col>
            <p>
              Have an account already?
            </p> 
              <Link to={SignInPath}>
                Sign In!
              </Link>
            
          </Col>
      </div>
    );
  }
}

export default connect(
  state => ({
    fetching: !!state.authorization.fetching,
    errorMessage: state.authorization.message
  }),
  dispatch => ({
    onSignUpButton: (userData) => dispatch(signUp(userData)),
    onInitAuthState: () => {
      dispatch(initAuthorization());
    }
  })
)(SignUpPage);
