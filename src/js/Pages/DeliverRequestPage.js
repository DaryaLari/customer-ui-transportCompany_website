import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Button} from 'react-materialize'
import InputForm from 'js/Elements/Common/InputForm'
import { extendProps, PageProps, ButtonProps } from 'constants/StylesCSS'

import { createDelivery, initDeliveryRequest } from 'actions/dispatchedActions/index'
import { RequestDeliveryFormParams } from 'constants/LogicalConstants'
import DeliveryAgreement from 'js/Elements/DeliveryAgreement'

class DeliverRequestPage extends Component {
  agreementId = 'delivery-agreement';
  constructor(props) {
    super(props);
    
    this.state = {
      deliveryRequested: false
    };
  }
  processDeliveryRequest(params){
    this.data = {
      del_name: params.del_name,
      urgency: params.urgency,
      type: params.type,
      src_address: params.src_address.address_components
                    .reverse().map((item) => {return item.long_name}).join(', '),
      src_address_2: (params.src_address_2 ? params.src_address_2 : ""),
      src_latitude: params.src_address.geometry.location.lat(),
      src_longitude: params.src_address.geometry.location.lng(),
      dest_address: params.dest_address.address_components
                    .reverse().map((item) => {return item.long_name}).join(', '),
      dest_address_2: (params.dest_address_2 ? params.dest_address_2 : ""),
      dest_latitude: params.dest_address.geometry.location.lat(),
      dest_longitude: params.dest_address.geometry.location.lng()
    }
    window.$('#' + this.agreementId).modal('open');
    this.setState({deliveryRequested: true});
  }

  render() {
    return (
      <div id="deliveryrequest-page" {...extendProps(PageProps, "className", " center-align ")}>
        <div className="container page-content">
          <InputForm 
              {...RequestDeliveryFormParams}
              onSubmit={(params) => {this.processDeliveryRequest(params)}}
              fetching={this.props.fetching}
              disabled={this.props.fetching || this.props.requestSuccess}
              hiddenBtn={this.props.requestSuccess}
          />
          {
            this.state.deliveryRequested &&
            (<DeliveryAgreement modalId={this.agreementId} onAcceptAgreement={() => this.props.onRequestDeliverySubmit(this.data)} />)
          }
          <div>
            {
              this.props.requestSuccess && 
              (<div>
                  <p>Your request has been successfully delivered to the server</p>
                  <Button 
                      {...ButtonProps}
                      onClick={() => this.props.onInitRequestDeliveryState()} 
                  >
                  Request New Delivery
                  </Button> 
                </div>)
            }{
              this.props.requestSuccess === false &&
              <p className="errorMessage">{this.props.errorMessage}</p>
            }
          </div>
        </div>
      </div>
    );
  }
 }

 export default connect(
  state =>({
      requestSuccess: state.requestDelivery.success,
      fetching: state.requestDelivery.fetching,
      errorMessage: state.requestDelivery.message
  }),
  dispatch =>({
    onRequestDeliverySubmit: (params) => {
      dispatch(createDelivery(
          params
      ));
    },
    onInitRequestDeliveryState: () => {
        dispatch(initDeliveryRequest());
    },
  })
  )(DeliverRequestPage);
