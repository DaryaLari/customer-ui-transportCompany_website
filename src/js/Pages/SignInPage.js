import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Col } from 'react-materialize'
import InputForm from 'js/Elements/Common/InputForm'

import { signIn, initAuthorization } from 'actions/dispatchedActions/index'
import { PageProps } from 'constants/StylesCSS'
import { SignInFormParams } from 'constants/LogicalConstants'

var SignUpPath = require('constants/AppPaths').PagesPaths.SignUpPage;

class SignInPage extends Component {
  componentWillMount(){
    this.props.onInitAuthState();
  }

  render() {
    return (
      <div id="signin-page" {...PageProps}>
          <InputForm 
            {...SignInFormParams} 
            onSubmit={(params) => {this.props.onSignInButton(params)}}
            fetching={this.props.fetching}
            disabled={this.props.fetching} 
            hiddenBtn={this.props.fetching} 
          />
          { 
            this.props.errorMessage && 
            <p className="errorMessage">
              {this.props.errorMessage}
            </p>
          }
          <Col>
            <p>
              Don't have an account yet?
            </p> 
              <Link to={SignUpPath}>
                Sign Up!
              </Link>
            
          </Col>
      </div>
    );
  }
}

export default connect(
  state => ({
    fetching: !!state.authorization.fetching,
    errorMessage: state.authorization.message
  }),
  dispatch => ({
    onSignInButton: (userData) => dispatch(signIn(userData)),
    onInitAuthState: () => {
      dispatch(initAuthorization());
    }
  }),
)(SignInPage);
