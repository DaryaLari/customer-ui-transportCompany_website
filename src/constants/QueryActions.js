/*
* Describes names of dispatched actions
*/

// features used for converting obects with actions names of form:
//      action : RequestSuccessFail
// to
//      action: {
//          Request: 'actionRequest',
//          Success: 'actionSuccess',
//          Fail: 'actionFail'
//      }
const expand = (x, prefix) => x == null ? prefix : Object.keys(x)
.reduce((a, key) => ({...a, [key]: expand(x[key], `${prefix}.${key}`)}), {});
const expandRoot = (x, prefix) => [expand(x, prefix)]
.map((a) => Object.keys(x).forEach((key) => x[key] = a[key]));

const RequestSuccessFail = {
    Request: null,
    Success: null,
    Fail: null,
};

const SignupActions = {
    signIn: RequestSuccessFail,
    signUp: RequestSuccessFail,
    signOut: RequestSuccessFail,
};
expandRoot(SignupActions, "SignupActions");

const ApiRequestActions = {
    getParcelsList: RequestSuccessFail,
    createDelivery: RequestSuccessFail,
    getConfirmationCode: RequestSuccessFail,
    submitFeedback: RequestSuccessFail,
    getArchivedParcelsList: RequestSuccessFail
}

expandRoot(ApiRequestActions, "ApiRequestActions");

const InitStoreStateActions = {
    Authorization: "AuthorizationInit",
    ParcelsList: "ParcelsListInit",
    RequestDelivery: "RequestDeliveryInit",
    ArchivedParcelsList: "ArchivedParcelsListInit"
}

const initStoreReduxAction = "@@INIT"

export { SignupActions, ApiRequestActions, InitStoreStateActions, initStoreReduxAction }