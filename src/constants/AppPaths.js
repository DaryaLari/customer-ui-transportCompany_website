/*
* Contains used server URIs and web-app paths
*/

const NativeAppPaths = {
    AuthorizationPaths: {
        SignIn: "/customer/sign/in",
        SignUp: "/customer/register",
        SignOut: "/customer/log/out"
    },
    ApiPaths: {
        CreateDelivery: "/customer/create/delivery",
        GetParcelsList: "/customer/get/deliveries",
        GetArchivedParcelsList: "/customer/get/archived/deliveries",
        GetConfirmationCode: "/customer/confirmation_number/get",
        SubmitFeedback: "/customer/assess/delivery"
    }
}

export const RootPath = 'http://18.220.72.184:4377'

export const PagesPaths = {
    App: "/",
    HomePage: "/home",
    SignInPage: "/signin",
    SignUpPage: "/signup",
    ParcelsPage: "/parcels",
    DeliveryRequestPage: "/deliver_request",
    ArchivedParcelsPage: "/archive"
}

export default NativeAppPaths;