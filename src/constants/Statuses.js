/*
* Contains expanded description of statuses for different operations, which can be shown to user.
*/

const expand = (x) => Object.keys(x).map((key) => x[key].statusText = key);

// server response statuses
const queryStatus = {
    ok: {
        description: 'Operation was successfully completed'
    },   
    no_email: {
        description: 'No user with such email registered in the system'
    },    
    wrong_pswd: {
        description: 'Wrong password'
    },    
    wrong_tok: {
        description: 'Token for this operation is incorrect'
    },    
    int_error: {
        description: 'Some internal error happened on the server'
    },
    registered_alr: {
        description: 'User with such email or other unique parameter is already registered'
    },  
    no_parcel: {
        description: 'No parcel with such parameters can be found'
    }   
}

expand(queryStatus);

// status of delivery
const deliveryStatus = {
    Created: {
        description: 'Created'
    },   
    ConfirmedByOperator: {
        description: 'Confirmed by operator'
    },    
    ConfirmedByDriver: {
        description: 'Confirmed by driver'
    },    
    PickedUp: {
        description: 'In route'
    },    
    Delivered: {
        description: 'Delivered'
    },
    AssessedByCustomer: {
        description: 'Assesed'
    },
    Archived: {
        description: 'Archived'
    },
    Rejected: {
        description: 'Rejected by Operator'
    }
}

expand(deliveryStatus);

export { queryStatus, deliveryStatus }