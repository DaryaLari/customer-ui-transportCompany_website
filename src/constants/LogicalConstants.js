/*
* Constants used in JS React Components,
* such like options of dropDown picker, params for creating input forms
*/

const UrgencyDeliveryOptions = [
    'low', 
    'medium',
    'high'
];
const TypeDeliveryOptions = [
    'usual', 
    'special'
];

const SignUpFormParams = {
    title: "Sign up",
    inputs: {
      name: {
        type: "text",
        label: "Name",
      },
      phone: {
        type: "tel",
        label: "Phone number",
        validationRules: {
          required: true,
          numeric: true,
          min: 5,
          max: 11
        }
      },
      email: {
        type: "email",
        label: "E-mail",
        validationRules: {
          required: true,
          email: true
        }
      },
      password: {
        type: "password",
        label: "Password",
        validationRules: {
          required: true,
          min: 5,
          max: 15
        }
      }
    },
    onSubmit: (params) => {this.props.onSignUpButton(params)},
    button: {
      name: "Sign Up",
      className: "orange"
    }
}

const SignInFormParams = {
    title: "Sign in",
    inputs: {
      email: {
        type: "email",
        label: "E-mail",
        validationRules: {
          required: true,
          email: true
        }
      },
      password: {
        type: "password",
        label: "Password",
        validationRules: {
          required: true,
          min: 5,
          max: 15
        }
      }
    },
    onSubmit: (params) => {this.props.onSignInButton(params)},
    button: {
      name: "Sign In",
      className: "green"
    }
}

const RequestDeliveryFormParams = {
    title: "New Delivery",
    inputs: {
        del_name: {
          type: "text",
          label: "Name of the person, who will recieve the delivery",
          validationRules: {
              required: true,
              min: 2
          }
        },
        src_address: {
          type: "text",
          label: "Where to deliver the parcel from",
          validationRules: {
              required: true
          },
          autocomplete: 'places'
        },
        src_address_2: {
          type: "text",
          label: "Name/number of office or flat for source of delivery"
        },
        dest_address: {
          type: "text",
          label: "Where to deliver the parcel to",
          validationRules: {
              required: true
          },
          autocomplete: 'places'
        },
        dest_address_2: {
          type: "text",
          label: "Name/number of office or flat for destination of delivery"
        },
        urgency: {
          type: "select",
          label: "Urgency",
          options: UrgencyDeliveryOptions
        },
        type: {
        type: "select",
        label: "Type",
        options: TypeDeliveryOptions
        }
    },
    onSubmit: (params) => {this.processDeliveryRequest(params)},
    button: {
      name: "Request Delivery",
      className: "orange",
    }
}   

export { SignUpFormParams, SignInFormParams, RequestDeliveryFormParams }