/*
* Commonly user styles of components.
* Separated for easier changing them in the whole project at ones
*/

export const PageProps = {
    className: " page-content section container z-depth-5 hoverable center-align "
}
export const ClassNameCentered = " center-align "

export const ButtonProps = {
    className: " waves-effect waves-light btn-large col s4 "
}

export const InputProps = {
    className: "",
    s: 8
}

export const extendProps = (object, propName, extention) => {
    object[propName] = (object[propName] ? object[propName] : "") + extention;
    return object;
}