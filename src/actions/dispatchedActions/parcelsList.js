/*
* Features for receiving a list of existing user's parcels.
* Depending on the status of performed operation, they dispatch sertain events, 
* which are handled in a corresponding reducer
*/

import { assertApiError, isAuthenticationProblem, get } from 'actions/requests'
import { requiredUserData } from 'actions/user'
import { errMsgForUser } from 'actions/utils'
import { signOut } from 'actions/dispatchedActions/index'
const getParcelsListAction = require('constants/QueryActions').ApiRequestActions.getParcelsList;
const GetParcelsListPath = require('constants/AppPaths').default.ApiPaths.GetParcelsList;
const RootPath = require('constants/AppPaths').RootPath;

export const getParcelsList = () => (dispatch) => {
  dispatch({type: getParcelsListAction.Request});
  get(RootPath + GetParcelsListPath, requiredUserData())
  .then(assertApiError)
  .then((res) => {
    dispatch({type: getParcelsListAction.Success, result: res.result})
  })
  .catch((err) =>
      isAuthenticationProblem(err)
      .then(() => {dispatch(signOut())})
      .catch(
        (err) => {
          dispatch({
          type: getParcelsListAction.Fail, 
          message: errMsgForUser(err)
          })
        }
      )
  );
};

const getArchivedParcelsListAction = require('constants/QueryActions').ApiRequestActions.getArchivedParcelsList;
const GetArchivedParcelsListPath = require('constants/AppPaths').default.ApiPaths.GetArchivedParcelsList;

export const getArchivedParcelsList = () => (dispatch) => {
  dispatch({type: getArchivedParcelsListAction.Request});
  get(RootPath + GetArchivedParcelsListPath, requiredUserData())
  .then(assertApiError)
  .then((res) => {
    dispatch({type: getArchivedParcelsListAction.Success, result: res.result})
  })
  .catch((err) =>
      isAuthenticationProblem(err)
      .then(() => {dispatch(signOut())})
      .catch(
        (err) => {
          dispatch({
          type: getArchivedParcelsListAction.Fail, 
          message: errMsgForUser(err)
          })
        }
      )
  );
};