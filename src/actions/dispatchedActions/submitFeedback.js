/*
* Features for sending user's feedback for finished delivery.
* Depending on the status of performed operation, they dispatch sertain events, 
* which are handled in a corresponding reducer
*/

import { assertApiError, isAuthenticationProblem, put } from 'actions/requests'
import { requiredUserData } from 'actions/user'
import { errMsgForUser } from 'actions/utils'
import { signOut } from 'actions/dispatchedActions/index'

var submitFeedbackAction = require('constants/QueryActions').ApiRequestActions.submitFeedback;
var SubmitFeedbackPath = require('constants/AppPaths').default.ApiPaths.SubmitFeedback;
var RootPath = require('constants/AppPaths').RootPath;

export function submitFeedback(params) {
    return (dispatch) => {
        dispatch({type: submitFeedbackAction.Request});
        Object.assign(params, requiredUserData());
        console.log(params);
        put(RootPath + SubmitFeedbackPath, params)
            .then(assertApiError)
            .then((res) => dispatch({
                type: submitFeedbackAction.Success
            }))
            .catch((err) =>
            isAuthenticationProblem(err)
                .then(() => {
                    console.log(err);
                    dispatch(signOut());
                })
                .catch((err) => dispatch({
                type: submitFeedbackAction.Fail, 
                message: errMsgForUser(err)
            })));
    }
}