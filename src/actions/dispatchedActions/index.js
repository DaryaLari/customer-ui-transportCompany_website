/*
* Facade for accessing all possible actions (containing communication with server side of the app).
* For easier import all of them from one file
*/

import { initAuthorization, initDeliveryRequest } from 'actions/dispatchedActions/initStoreState'
import { getParcelsList, getArchivedParcelsList } from 'actions/dispatchedActions/parcelsList'
import { createDelivery } from 'actions/dispatchedActions/requestDelivery'
import { signIn, signUp, signOut } from 'actions/dispatchedActions/authorization'
import { submitFeedback } from 'actions/dispatchedActions/submitFeedback'

export { getParcelsList, getArchivedParcelsList, createDelivery, signIn, signUp, signOut, initAuthorization, initDeliveryRequest, submitFeedback }