/*
* Features for sending user's request on creating new delivery.
* Depending on the status of performed operation, they dispatch sertain events, 
* which are handled in a corresponding reducer
*/

import { assertApiError, isAuthenticationProblem, put } from 'actions/requests'
import { requiredUserData } from 'actions/user'
import { errMsgForUser } from 'actions/utils'
import { signOut } from 'actions/dispatchedActions/index'

var createDeliveryAction = require('constants/QueryActions').ApiRequestActions.createDelivery;
var CreateDeliveryPath = require('constants/AppPaths').default.ApiPaths.CreateDelivery;
var RootPath = require('constants/AppPaths').RootPath;

export function createDelivery(params) {
    return (dispatch) => {
        dispatch({type: createDeliveryAction.Request});
        Object.assign(params, requiredUserData());
        put(RootPath + CreateDeliveryPath, params)
            .then(assertApiError)
            .then((res) => dispatch({
                type: createDeliveryAction.Success
            }))
            .catch((err) =>
            isAuthenticationProblem(err)
                .then(() => dispatch(signOut()))
                .catch((err) => dispatch({
                type: createDeliveryAction.Fail, 
                message: errMsgForUser(err)
            })));
    }
}