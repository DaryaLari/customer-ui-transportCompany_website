/*
* Features for initializing states of entities in app's storage.
* They dispatch 'init state' events, 
* which are handled in a corresponding reducer
*/

import { InitStoreStateActions } from 'constants/QueryActions'

export function initAuthorization() { 
    return (dispatch) => {
        dispatch({type: InitStoreStateActions.Authorization});
    }
}

export function initDeliveryRequest() { 
    return (dispatch) => {
        dispatch({type: InitStoreStateActions.RequestDelivery});
    }
}