/*
* Features for performing user authorization.
* Depending on the status of performed operation, they dispatch sertain events, 
* which are handled in a corresponding reducer
*/

import { assertApiError, get, put } from 'actions/requests'
import { push } from 'react-router-redux'
import { requiredUserData } from 'actions/user'
import { SignupActions } from 'constants/QueryActions'
import NativeAppPaths, { RootPath, PagesPaths } from 'constants/AppPaths'
import { errMsgForUser } from 'actions/utils'

const AuthorizationPaths = NativeAppPaths.AuthorizationPaths;

export const signIn = ({email, password}) => (dispatch) => {
    dispatch({type: SignupActions.signIn.Request});

    get(RootPath + AuthorizationPaths.SignIn, {email, password})
        .then(assertApiError)
        .then((res) => {
            dispatch(
            {
                type: SignupActions.signIn.Success, 
                user: getUser(email, res.result)
            }); 
            dispatch(push(PagesPaths.HomePage));
        })
        .catch((err) => {dispatch({type: SignupActions.signIn.Fail, message: errMsgForUser(err)})});
};

export const signUp = (userData) => (dispatch) => {
    dispatch({type: SignupActions.signUp.Request});
    
    put(RootPath + AuthorizationPaths.SignUp, userData)
        .then(assertApiError)
        .then((res) => {
            dispatch(
            {
                type: SignupActions.signUp.Success, 
                user: getUser(userData.email,  res.result)
            }
            );
            dispatch(push(PagesPaths.HomePage));
        })
        .catch((err) => {dispatch({type: SignupActions.signUp.Fail, message: errMsgForUser(err)})});
};

export const signOut = () => (dispatch) => {
    dispatch({type: SignupActions.signOut.Request});

    dispatch(push(PagesPaths.HomePage));
    put(RootPath + AuthorizationPaths.SignOut, requiredUserData())
        .then(assertApiError)
        .then((res) => {
            dispatch({type: SignupActions.signOut.Success});
        })
        .catch((err) => {dispatch({type: SignupActions.signOut.Fail, message: errMsgForUser(err)})});
};

const getUser = (email, {token, id}) =>
    ({token, email, id});

