/*
* Some small scripts
*/

export const exclusive = (f) => (e) => {
    e.preventDefault();
    f && f(e);
};

var queryStatus = require('constants/Statuses').queryStatus;
export function errMsgForUser(error) {
    if(error.status){
        for (var key in queryStatus){
            let status = queryStatus[key];
            if(status.statusText === error.status) {
                return status.description;
            }
        }
    }
    return "Some error occured";
};