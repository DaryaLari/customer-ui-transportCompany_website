/*
* Features connected with user data and his state in the app's storage
*/

import { store } from "index";

// returns user data, which are needed as parameters in server requests, which require user authentication (ex. 'token')
export function requiredUserData(){
    let user = store.getState().authorization.user;
    return user ? {token: user.token} : null;
}