/*
* Templates for sending different types of requests to server 
* and methods for handling communication errors
*/

import { queryStatus } from 'constants/Statuses'

export const get = (url, args) => fetch(`https://j9132jd38r230.herokuapp.com/${url}?${new URLSearchParams(args)}`)
  .then(x => x.json())
  .catch(x => console.log(x));

const packFormData = (args) => {
  const result = new FormData();
  Object.keys(args).forEach(x => result.append(x, args[x]));
  return result;
};

export const put = (url, args) => fetch(`https://j9132jd38r230.herokuapp.com/${url}`, {
    method: "PUT",
    body: packFormData(args),
  })
  .then(x => x.json())
  .catch(x => console.log(x));

// checks existing of error
export const assertApiError = (res) => {
  return res.status === queryStatus.ok.statusText ? 
  Promise.resolve(res) : Promise.reject(res);
}

// checks if the error is cased because of wrong token
export const isAuthenticationProblem = (res) => {
  return res.status === queryStatus.wrong_tok.statusText ? 
  Promise.resolve(res) : Promise.reject(res);
}