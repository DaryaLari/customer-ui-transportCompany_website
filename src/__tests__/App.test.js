'use strict'
import React from 'react'
import ReactDOM from 'react-dom'
import { expect } from 'chai'
import { shallow, mount, render } from 'enzyme'
import {Provider} from 'react-redux'

const {describe, it} = require('mocha')

import App from '../App'
import { store } from '../index'

const wrapper = shallow(<App />);

describe('(Component) App', () => {
  it('renders...', () => {
    expect(wrapper).to.have.length(1);
  });
});
