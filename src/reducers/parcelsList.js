/*
* Changes store state of parcels list on sertain event
*/

import { parcelsListInitState } from 'settings/storeInitState';
import { initStoreReduxAction, ApiRequestActions } from 'constants/QueryActions'
const getParcelsListAction = ApiRequestActions.getParcelsList;

export default function parcelsList(state = parcelsListInitState, action){
  switch(action.type){
    case initStoreReduxAction:
      return state;
    case getParcelsListAction.Request:
      return {
        ...state,
        parcels: null,
        fetching: true,
        message: null
      };
    case getParcelsListAction.Success:
      return {
        ...state,
        parcels: action.result,
        fetching: false,
        message: null
            };
    case getParcelsListAction.Fail:
      return {
        ...state,
        parcels: null,
        fetching: false,
        message: action.message
      };
    default:
      return state;
  }
}
