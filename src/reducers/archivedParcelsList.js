/*
* Changes store state of archived parcels list on sertain event
*/

import { archivedParcelsListInitState } from 'settings/storeInitState';
import { initStoreReduxAction, ApiRequestActions } from 'constants/QueryActions'
const getArchivedParcelsListAction = ApiRequestActions.getArchivedParcelsList;

export default function archivedParcelsList(state = archivedParcelsListInitState, action){
  switch(action.type){
    case initStoreReduxAction:
      return state;
    case getArchivedParcelsListAction.Request:
      return {
        ...state,
        parcels: null,
        fetching: true,
        message: null
      };
    case getArchivedParcelsListAction.Success:
      return {
        ...state,
        parcels: action.result,
        fetching: false,
        message: null
            };
    case getArchivedParcelsListAction.Fail:
      return {
        ...state,
        parcels: null,
        fetching: false,
        message: action.message
      };
    default:
      return state;
  }
}
