/*
* Changes store state of status for operation of requesting new delivery on sertain event
*/

import { requestDeliveryInitState } from 'settings/storeInitState'
import { initStoreReduxAction, InitStoreStateActions, ApiRequestActions } from 'constants/QueryActions'
const createDeliveryAction = ApiRequestActions.createDelivery;

export default function requestDelivery(state = requestDeliveryInitState, action){
   switch(action.type){
    case initStoreReduxAction:
      return state;
    case createDeliveryAction.Request:
      return {
        ...state,
        success: false,
        fetching: true,
        message: null
      };
    case createDeliveryAction.Success:
      return {
        ...state,
        success: true,
        fetching: false,
        message: null
      };
    case createDeliveryAction.Fail:
      return {
        ...state,
        success: false,
        fetching: false,
        message: action.message
      };
    case InitStoreStateActions.RequestDelivery:
      return requestDeliveryInitState;
    default:
      return state;
  }
}
