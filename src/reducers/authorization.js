/*
* Changes store state of user on sertain event
*/

import { SignupActions, initStoreReduxAction, InitStoreStateActions } from 'constants/QueryActions'
import { authInitState } from 'settings/storeInitState'

export default function authorization(state = authInitState, action) {
  switch(action.type){
    case initStoreReduxAction:
      return {...state, user: JSON.parse(localStorage.getItem("signup.user")), nonce: 6};

    case SignupActions.signUp.Request:
    case SignupActions.signIn.Request:
    case SignupActions.signOut.Request:
      return {...state, fetching: true, message: null};

    case SignupActions.signIn.Fail:
    case SignupActions.signUp.Fail:
      localStorage.setItem("signup.user", JSON.stringify(null));
      return {...state, fetching: false, message: action.message};

    case SignupActions.signUp.Success:
    case SignupActions.signIn.Success:
      localStorage.setItem("signup.user", JSON.stringify(action.user));
      return {...state, user: action.user, fetching: false};

    case SignupActions.signOut.Fail:
    case SignupActions.signOut.Success:
      localStorage.setItem("signup.user", null);
      return {...state, user: null, fetching: false};

    case InitStoreStateActions.Authorization:
      return authInitState;

    default:
      return state;
  }
}