/*
* Registers events' handlers of the app
*/

import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import authorization from 'reducers/authorization'
import parcelsList from 'reducers/parcelsList'
import requestDelivery from 'reducers/requestDelivery'
import submitFeedback from 'reducers/submitFeedback'
import archivedParcelsList from 'reducers/archivedParcelsList'


export default combineReducers({
  routing: routerReducer,
  authorization,
  parcelsList,
  requestDelivery,
  submitFeedback,
  archivedParcelsList
})
