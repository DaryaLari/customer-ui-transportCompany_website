/*
* Changes store state of status for operation of submiting feedback for delivery on sertain event
*/

import { submitFeedbackInitState } from 'settings/storeInitState'
import { initStoreReduxAction, InitStoreStateActions, ApiRequestActions } from 'constants/QueryActions'
const submitFeedbackAction = ApiRequestActions.submitFeedback;

export default function requestDelivery(state = submitFeedbackInitState, action){
   switch(action.type){
    case initStoreReduxAction:
      return state;
    case submitFeedbackAction.Request:
      return {
        ...state,
        success: false,
        fetching: true,
        message: null
      };
    case submitFeedbackAction.Success:
      return {
        ...state,
        success: true,
        fetching: false,
        message: null
      };
    case submitFeedbackAction.Fail:
      return {
        ...state,
        success: false,
        fetching: false,
        message: action.message
      };
    case InitStoreStateActions.submitFeedbackAction:
      return submitFeedbackInitState;
    default:
      return state;
  }
}
