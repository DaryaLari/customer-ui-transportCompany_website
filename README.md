Install Node.js Package Manager.

In the project directory, you can run:

### `npm install`
### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You can see the latest version of project from master branch hosted on:
https://deliverme.netlify.com/